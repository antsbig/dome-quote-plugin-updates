<?php
function genExelAlt($data_for_tbs) {

	// dome count
	$dome_requested = count($data_for_tbs['domes']);

	$tpl_filepath = __DIR__ . '/tbs/templates/electra-quote-tbs-tpl-'.$dome_requested.'.xlsx';

	/* init and load plugins */
	$TBS = new clsTinyButStrong;
	$TBS->Plugin(TBS_INSTALL, OPENTBS_PLUGIN);
	//$TBS->PlugIn(OPENTBS_RELATIVE_CELLS, true);

	/* before load template, we need VarRef (variable references) for 'onload' hook */
	/* i. check for phone number */
	$nophone = (empty($data_for_tbs['phone']) || !isset($data_for_tbs['phone']) || ($data_for_tbs['phone'] == ''))?true:false;
	$TBS->VarRef['nophone'] = $nophone;

	/* ii. dirty check for rows of item */
	/* Since we use a dirty approach that the separated template files, we don't use it for now */
	/*
	for ($i = 1; $i <= 5; $i++) {
		if($i <= $dome_requested) {
			$TBS->VarRef['item_'.$i] = true;
		} else {
			$TBS->VarRef['item_'.$i] = false;
		}
	}
	*/

	/* uncomment below for to load the template with utf8 supported templating */
	//$TBS->LoadTemplate($tpl_filepath, OPENTBS_ALREADY_UTF8);

	$TBS->LoadTemplate($tpl_filepath);

	/* date */
	$TBS->VarRef['today'] = date("F j, Y");
	$TBS->VarRef['valid'] = date("F j, Y", strtotime("+30 days"));


	/* assign variable references and merge blocks */
	$TBS->VarRef['name'] = $data_for_tbs['name'];
	$TBS->VarRef['company'] = $data_for_tbs['company'];
	$TBS->VarRef['email'] = $data_for_tbs['email'];
	if(!$nophone) {
		$TBS->VarRef['phone'] = $data_for_tbs['phone'];
	}
	$TBS->VarRef['total'] = $data_for_tbs['total'];


	$dome_count = 1;
	foreach($data_for_tbs['domes'] as $dome) {

		// title of group
		$TBS->VarRef['data_title_'.$dome_count] = 'DOME ' . $dome['Domes'][0][1] . ' - ' . $dome['Domes'][2][1];

		/**
		 * Uncomment to use this block of code for Automatic cell merging array with xlsx
		 * @internal automatic merging with given template doesn't seem to be working, because of custom cell merging rows by rows
		 * @todo need tow work on excel sheet to be consistant on rows, cols and merging (i.e. design)
		 */
		/*
		$xlsx_vars['cells'] = array();
		$xlsx_vars['cells'][] = array('description' => 'Dome ' . $dome['Domes'][0][1], 'price' => $dome['Domes'][0][2]);
		$xlsx_vars['cells'][] = array('description' => 'Dome Cover: ' . $dome['Domes'][1][1], 'price' => $dome['Domes'][1][2]);
		$xlsx_vars['cells'][] = array('description' => 'Type of surface/ground: ' . $dome['Domes'][4][1], 'price' => $dome['Domes'][4][2]);
		$xlsx_vars['cells'][] = array('description' => 'AC Solution', 'price' => $dome['Domes'][5][2]);
		*/

		/**
		 * This is much manual method, but for the given excel sheet with lots of custom merged cells, this works simply
		 */
		/* i. size */
		$TBS->VarRef['c_size_'.$dome_count.'_description'] = 'Dome ' . $dome['Domes'][0][1];
		$TBS->VarRef['c_size_'.$dome_count.'_price'] = intval($dome['Domes'][0][2]);
		/* ii. cover */
		$TBS->VarRef['c_cover_'.$dome_count.'_description'] = 'Dome Cover: ' . $dome['Domes'][1][1];
		$TBS->VarRef['c_cover_'.$dome_count.'_price'] = intval($dome['Domes'][1][2]);
		/* iii. type */
		$TBS->VarRef['c_type_'.$dome_count.'_description'] = 'Type of surface/ground: ' . $dome['Domes'][4][1];
		$TBS->VarRef['c_type_'.$dome_count.'_price'] = intval($dome['Domes'][4][2]);
		/* iv. ac */
		$TBS->VarRef['c_ac_'.$dome_count.'_description'] = 'AC Solution';
		$TBS->VarRef['c_ac_'.$dome_count.'_price'] = intval($dome['Domes'][5][2]);


		/* Uncomment this if dynamic columns to be merged with data automatically */
		//$TBS->MergeBlock('data_cells_'.$dome_count, $xlsx_vars['cells']);

		$dome_count += 1;
	}

	/* Upload path definition */
	$xlsx_filename = saveExcelToLocalFileAlt();

	//$TBS->PlugIn(OPENTBS_DISPLAY_SHEETS, 'Quote');
	$TBS->Show(OPENTBS_FILE, $xlsx_filename[1]);

	return $xlsx_filename;
}

function saveExcelToLocalFileAlt() {
    $upload_dir = wp_upload_dir();
	$file = "/electra-dome-proposal-".date("Y-m-d_H-i",time()).".xlsx";
    return [$upload_dir['url'] . $file, $upload_dir['path'] . $file];
}
