# electra-exhibitions.com: Dome Quote Wordpress Plugin Patch

## Update Guide for Change log on: 16th Feb, 2017

Template Engine for generating Excel file on Quotation request via online, has been migrated to TinyButStrong OpenTBS Plugin from PHPExcel.

Below is the update guide for website developer.

### 1. Upload Template-Engine files and custom Excel-XSLX templates

* Upload the **tbs** directory to plugin's root directory *i.e. /wp-content/plugins/electra-dome-quote/tbs*
* Open the plugin file **dome-quote.php** inside plugin root directory *i.e. /wp-content/plugins/electra-dome-quote/dome-quote.php*

### 2. Update the plugin-file codes for Template-Engine migration
* Insert following code at the beginning of plugin file **dome-quote.php**
```
#!php
require_once( __DIR__ . '/tbs/tbs_us/tbs_class.php' );
require_once( __DIR__ . '/tbs/tbs_plugin_opentbs_1.9.8/tbs_plugin_opentbs.php' );
```
* Copy all the `functions` inside [**update.php**](update.php) file and paste those copied functions into **dome-quote.php** plugin file. *Note: update.php file is located at the same directory of this READEME.md markdown file*

    1. **genExelAlt** PHP function
    2. **saveExcelToLocalFileAlt** PHP function

### 3. Update existing plugin-file functions to be effected by new Template-Engine functionality.

* Locate the `function` named **programmatically_create_post** (~ on line *87*)
* Find the `if` condition starting with `if(!is_wp_error($post_id))` (~ on line *152*)
* Comment or Remove following line within above `if` block: `$file_url = genExel( $excel_raw_data );` (~ on line *153*)
* Comment or Remove following two lines within above `if` block (~ on line numbers *165, 166*):
```
#!php
update_field('xlsx_file', $file_url[0], $post_id);
update_post_meta($post_id, 'xlsx_file_path', $file_url[1]);
```
* Paste following 4 lines after above lines
```
#!php
$data_for_tbs = get_post_meta($post_id, 'all_qoutations', true);
$xlsx_filename = genExelAlt($data_for_tbs);
update_field('xlsx_file', $xlsx_filename[0], $post_id);
update_post_meta($post_id, 'xlsx_file_path', $xlsx_filename[1]);
```
